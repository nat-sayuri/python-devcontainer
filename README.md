# Python Devcontainer

Project to create a Docker container for Python development.

Author: Natasha Nakashima
Email: natashasayuri@gmail.com

## Files

This project has two main directories:

1. .devcontainer: with Dockerfile, docker-compose.yml and configuration file for Visual Studio Code Remote - Containers extension (devcontainer.json)
2. workspace: directory that is shared with the python-devcontainer that is created

OBS: Bind mount between: (host) $PWD/workspace <-> (container) /workspace

OBS: User name: vscode
     User password: vscode

OBS: Uncomment lines 28-32 from Dockerfile to add new packages inside the Docker image.

## Clone this repository

If you want to change the files in this project, clone the repository using git clone.

```bash
git clone https://bitbucket.org/nat-sayuri/python-devcontainer.git
```

## Usage

1. Start VS Code and the workspace for the python-devcontainer.
2. Start Remote - Containers

## Some useful links

1. [Python in Visual Studio Code](https://code.visualstudio.com/docs/languages/python)

2. [Getting Started with Python in VS Code: Hello World!](https://code.visualstudio.com/docs/python/python-tutorial)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

